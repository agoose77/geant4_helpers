#ifndef HCE_OPERATOR
#define HCE_OPERATOR

#include <boost/python/numpy.hpp>
#include <boost/python.hpp>

#include <iostream>
#include <vector>

#include "Randomize.hh"
#include "globals.hh"
#include "Hit.hh"

using namespace boost::python;
using namespace boost::python::numpy;
using namespace CLHEP;


G4double sumEnergyDeposited(const HitsCollection &);
G4int countOfType(const HitsCollection&, const G4String&);
template <typename T>
ndarray ndarray_from_vec(std::vector<T> vec);
ndarray getEDepArray(const HitsCollection &);
ndarray getETotArray(const HitsCollection &);
ndarray getPDGArray(const HitsCollection &);
ndarray getTimeArray(const HitsCollection &hc);
ndarray getTrackIDArray(const HitsCollection &hc);
tuple getProcessData(const HitsCollection &hc);
ndarray getRandArray(ndarray &energies, double yield);

HitsCollection* makeHC(int);

HitsCollection* getHCFromVirtual(G4VHitsCollection *coll)
{
    return dynamic_cast<HitsCollection*>(coll);
}

#endif // HCE_OPERATOR