#include "G4UserTrackingAction.hh"
#include "TrackingAction.hh"
#include "G4Track.hh"
#include "G4VProcess.hh"
#include "G4ios.hh"
#include "G4IonTable.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4EventManager.hh"
#include "G4TrackingManager.hh"
#include <cassert>


TrackingAction::TrackingAction(G4int mass_, G4int atomic_) :
        G4UserTrackingAction(), atomic_mass(mass_), atomic_number(atomic_)
{

};

TrackingAction::~TrackingAction()
{
};

void TrackingAction::EndOfEvent()
{

}

void TrackingAction::PostUserTrackingAction(const G4Track* track)
{
}

void TrackingAction::PreUserTrackingAction (const G4Track *track)
{
    G4ParticleDefinition* particle = track->GetDefinition();
    if (particle->GetAtomicNumber() == atomic_number && particle->GetAtomicMass() == atomic_mass && particle->GetPDGLifeTime() > 0.0)
    {
        auto tr = (G4Track*) track;
        tr->SetTrackStatus(fStopAndKill);
    }
}
