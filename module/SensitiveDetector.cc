#include "SensitiveDetector.hh"
#include "G4VProcess.hh"


SensitiveDetector::SensitiveDetector(G4String name) : G4VSensitiveDetector(name), fHitsCollection(NULL), HCID(-1),
    timeWindow(0.0)
{
    collectionName.insert(name);
}

SensitiveDetector::~SensitiveDetector(){}


void SensitiveDetector::Initialize(G4HCofThisEvent* HCE)
{
    fHitsCollection =  new HitsCollection(SensitiveDetectorName, collectionName[0]);
    //Store collection with event and keep ID
    if (HCID < 0) {
        HCID = GetCollectionID(0);
    }

    HCE->AddHitsCollection(HCID, fHitsCollection); 
    }


G4bool SensitiveDetector::ProcessHits(G4Step* step, G4TouchableHistory* history)
{
    G4Track* track = step->GetTrack();
    G4StepPoint* pre_step = step->GetPreStepPoint();
    G4StepPoint* post_step = step->GetPostStepPoint();

    MHit* hit = new MHit();
    hit->SetEDep(step->GetTotalEnergyDeposit());
    hit->SetETot(pre_step->GetTotalEnergy());
    hit->SetTime(post_step->GetGlobalTime());
    hit->SetParticleID(track->GetDefinition()->GetPDGEncoding());
    hit->SetTrackID(track->GetTrackID());

    G4ThreeVector pos = post_step->GetPosition();
    hit->SetPos(pos);
    hit->SetLocalPos(pre_step->GetTouchableHandle()->GetHistory()->GetTopTransform().TransformPoint(pos));

    if (track->GetCreatorProcess() != NULL)
    {
        hit->SetProcessName(track->GetCreatorProcess()->GetProcessName());
    }
    G4VTouchable* touchable = (G4VTouchable*) pre_step->GetTouchable();
    hit->SetVolumeName(touchable->GetVolume(0)->GetName());

    // Creating the hit and add it to the collection
    fHitsCollection->insert(hit);
    return true;
}