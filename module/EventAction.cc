#include "G4UserEventAction.hh"
#include "EventAction.hh"


EventAction::EventAction(TrackingAction* action): trackingAction(action)
{
    
};

EventAction::~EventAction()
{
};


void EventAction::EndOfEventAction(const G4Event* event)
{
    trackingAction->EndOfEvent();
};