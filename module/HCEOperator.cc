//
// Created by angus on 07/12/17.
//
#include <boost/python/numpy.hpp>
#include <boost/python.hpp>
#include <boost/python/list.hpp>
#include <iostream>
#include <unordered_map>
#include <stdlib.h>
#include <vector>
#include <string>
#include <cstdint>
#include <algorithm>

#include "Randomize.hh"
#include "G4ParticleTable.hh"
#include "G4Poisson.hh"
#include "globals.hh"
#include "Hit.hh"

using namespace CLHEP;
using namespace boost::python;
using namespace boost::python::numpy;



ndarray getRandArray(ndarray &energies, double yield)
{
    int length = energies.shape(0);
    double* energy_array = reinterpret_cast<double*>(energies.get_data());
    double* result_array = new double[length];
    for (int i=0; i < length; i++)
    {
        double MeanNumberOfPhotons = yield*energy_array[i];

        G4int NumPhotons;

        if (MeanNumberOfPhotons > 10.)
        {
            G4double sigma = 1.0* std::sqrt(MeanNumberOfPhotons);
            NumPhotons = G4int(G4RandGauss::shoot(MeanNumberOfPhotons,sigma)+0.5);
        }
        else
        {
            NumPhotons = G4int(G4Poisson(MeanNumberOfPhotons));
        }

        result_array[i] = NumPhotons;
    }

    Py_intptr_t shape[1] = {length};
    ndarray result = empty(1, shape, dtype::get_builtin<double>());

    double* dest = reinterpret_cast<double*>(result.get_data());
    std::copy(result_array, result_array+length, dest);
    return result;
}


G4double sumEnergyDeposited(const HitsCollection &hc) {
    G4double energy = 0.0;
    for (int i = 0; i < hc.entries(); i++) {
        energy += hc[i]->GetEDep();
    }
    G4cout << hc.entries() << " entries" << std::endl;
}


G4int countOfType(const HitsCollection &hc, const G4String &particleName) {
    G4int count = 0;

    G4ParticleTable *table = G4ParticleTable::GetParticleTable();
    G4ParticleDefinition *definition = table->FindParticle(particleName);

    if (definition == NULL)
        throw std::invalid_argument("Need valid particle name");

    G4int pdg = definition->GetPDGEncoding();

    for (int i = 0; i < hc.entries(); i++) {
        MHit *hit = hc[i];
        if (hit->GetParticleID() == pdg)
            count++;
    }

    return count;
}


template<typename T>
ndarray ndarray_from_vec(std::vector<T> vec) {
    Py_intptr_t shape[1] = {vec.size()};
    ndarray result = empty(1, shape, dtype::get_builtin<T>());
        std::copy(vec.begin(), vec.end(), reinterpret_cast<T *>(result.get_data()));
    return result;
}


template <typename T>
ndarray ndarray_apply(const HitsCollection &hc, T (MHit::*ptr)())
{
    std::vector<T> values;
    for (int i = 0; i < hc.entries(); i++) {
        MHit *hit = hc[i];
        values.push_back((hit->*ptr)());
    }
    return ndarray_from_vec<T>(values);
}


ndarray getEDepArray(const HitsCollection &hc) {
    return ndarray_apply<double>(hc, &MHit::GetEDep);
}


ndarray getPDGArray(const HitsCollection &hc) {
    return ndarray_apply<int>(hc, &MHit::GetParticleID);
}

ndarray getETotArray(const HitsCollection &hc) {
    return ndarray_apply<double>(hc, &MHit::GetETot);
}

ndarray getTimeArray(const HitsCollection &hc) {
    return ndarray_apply<double>(hc, &MHit::GetTime);
}

ndarray getTrackIDArray(const HitsCollection &hc) {
    return ndarray_apply<int>(hc, &MHit::GetTrackID);
}






// Converts a C++ map to a python dict
template <class K, class V>
dict std_unordered_map_to_dict(std::unordered_map<K, V> map) {
    typename std::unordered_map<K, V>::iterator iter;
    dict dictionary;
    for (iter = map.begin(); iter != map.end(); ++iter) {
        dictionary[iter->first] = iter->second;
    }
    return dictionary;
}


HitsCollection* makeHC(int n)
{
    double time = 0.0;
    double dt = 1./60;
    int tid = 0;

    std::string process_names[3] = {"scintillation", "cherenkov", "other_proc"};


    HitsCollection *hc =  new HitsCollection("SomeSD", "SomeSD");
    for (int i=0; i<n; i++)
    {
        MHit* hit = new MHit();
        hit->SetEDep(random());
        hit->SetETot(random());
        hit->SetTime(time);
        time += dt;

        hit->SetParticleID(0);
        hit->SetTrackID(tid++);

        hit->SetProcessName(process_names[rand() % 3]);
        hit->SetVolumeName("some_vol");
        hc->insert(hit);
    }

    return hc;
}


tuple getProcessData(const HitsCollection &hc)
{
    uint8_t pid = 0;
    std::unordered_map<std::string, uint8_t> map;
    std::vector<uint8_t> values;

    for (int i=0; i< hc.entries(); i++)
    {
        MHit *hit = hc[i];
        std::string proc_name = hit->GetProcessName();

        std::unordered_map<std::string, uint8_t>::iterator it = map.find(proc_name);
        uint8_t this_pid;

        if (it == map.end()) {
            this_pid = pid;
            map[proc_name] = pid++;
        }
        else {
            this_pid = it->second;
        }
        values.push_back(this_pid);

    }
    return make_tuple(std_unordered_map_to_dict<std::string, uint8_t>(map),
                        ndarray_from_vec<uint8_t>(values));
}