#include "Hit.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4VisAttributes.hh"

G4ThreadLocal G4Allocator<MHit> *HitAllocator = 0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

MHit::MHit() : G4VHit() {
    colourHit    = G4Colour(1.0, 0.0, 0.0);
    colourPassby = G4Colour(0.0, 1.0, 0.0);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

MHit::~MHit() {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

MHit::MHit(const MHit &right) : G4VHit() {
    eDep = right.eDep;
    eTot = right.eTot;
    time = right.time;
    particleID = right.particleID;
    trackID = right.trackID;
    processName = right.processName;
    volumeName = right.volumeName;
    pos = right.pos;
    localPos = right.localPos;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

const MHit &MHit::operator=(const MHit &right) {
    eDep = right.eDep;
    eTot = right.eTot;
    time = right.time;
    particleID = right.particleID;
    trackID = right.trackID;
    processName = right.processName;
    volumeName = right.volumeName;
    pos = right.pos;
    localPos = right.localPos;

    return *this;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4int MHit::operator==(const MHit &right) const {
    return (this == &right) ? 1 : 0;
}


void MHit::Draw() {
    G4VVisManager *pVVisManager = G4VVisManager::GetConcreteInstance();
    if (pVVisManager) {
        G4Circle circle(pos);
        circle.SetFillStyle(G4Circle::filled);


        if (eTot > 0) {
            circle.SetVisAttributes(G4VisAttributes(colourHit));
            circle.SetScreenSize(5);
        } else {
            circle.SetVisAttributes(G4VisAttributes(colourPassby));
            circle.SetScreenSize(3);
        }
        pVVisManager->Draw(circle);
    }
}
