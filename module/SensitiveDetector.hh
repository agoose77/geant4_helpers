#ifndef SENSITIVE_DETECTOR_HH
#define SENSITIVE_DETECTOR_HH 1

#include "Hit.hh"
#include "G4VSensitiveDetector.hh"
#include <vector>

class G4Step;
class G4HCofThisEvent;


typedef std::vector<G4int> HitCountVector;


class SensitiveDetector : public G4VSensitiveDetector
{
  public:

    SensitiveDetector(const G4String name);
    virtual ~SensitiveDetector();

    virtual void Initialize(G4HCofThisEvent* );
    
    virtual G4bool ProcessHits(G4Step* aStep, G4TouchableHistory* );

 
  private:
    G4int HCID;
    HitsCollection* fHitsCollection;
    G4double timeWindow;
 
};

#endif