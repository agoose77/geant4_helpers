//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: pyTestEm0.cc 66241 2012-12-13 18:34:42Z gunter $
// ====================================================================
//   pyTestEm0.cc
//
//   python wrapper for user application
//                                         2007 Q
// ====================================================================
#include "SensitiveDetector.hh"
#include "HCEOperator.hh"
#include "Hit.hh"
#include "TrackingAction.hh"
#include "EventAction.hh"

#include "G4String.hh"
#include "G4UserTrackingAction.hh"
#include "G4UserEventAction.hh"

#include <boost/python.hpp>
#include <boost/python/list.hpp>

#include <vector>
#include <string>

using namespace boost::python;

// ====================================================================
//   Expose to Python
// ====================================================================

#include <string>
#include <sstream>


BOOST_PYTHON_MODULE (detector) {
    namespace np = boost::python::numpy;
    np::initialize();

    def("getRandArray", getRandArray);
    def("getPDGArray", getPDGArray);
    def("sumEnergyDeposited", sumEnergyDeposited);
    def("countOfType", countOfType);
    def("getEDepArray", getEDepArray);
    def("getETotArray", getETotArray);
    def("getProcessData", getProcessData);
    def("getTimeArray", getTimeArray);
    def("getTrackIDArray", getTrackIDArray);
    def("getHCFromVirtual", getHCFromVirtual, return_internal_reference<>());
    def("makeHC", makeHC, return_value_policy<manage_new_object>());

    class_<SensitiveDetector, SensitiveDetector*, bases<G4VSensitiveDetector>, boost::noncopyable>
            ("SensitiveDetector", "sensitive detector", no_init)
            // ---
            .def(init<const G4String &>());

    class_<HitsCollection, HitsCollection*, bases<G4VHitsCollection>, boost::noncopyable>
            ("HitsCollection", "HitsCollection class", no_init);
            // ---

    class_<TrackingAction, TrackingAction*, bases<G4UserTrackingAction>, boost::noncopyable>
            ("TrackingAction", "Tracking action to prevent chain decays", no_init)
            .def(init<G4int, G4int>())
            ;

    class_<EventAction, EventAction*, bases<G4UserEventAction>, boost::noncopyable>
            ("EventAction", "Event action to re-initialise tracking action", no_init)
            .def(init<TrackingAction*>())
            ;
            // ---
}
