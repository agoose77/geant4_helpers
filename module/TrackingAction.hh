#ifndef TRACKING_ACTION_HH
#define TRACKING_ACTION_HH

#include "G4UserTrackingAction.hh"
#include "globals.hh"
#include <unordered_map>

// class G4int;
class G4Track;
class G4TrackingManager;
class G4ParticleDefinition;


struct TrackInfo {
  G4int trackID;
  G4ParticleDefinition* definition;
  TrackInfo* parent;
};


class TrackingAction: public G4UserTrackingAction
{
public:
    TrackingAction(G4int, G4int);
    ~TrackingAction();

    G4int atomic_mass;
    G4int atomic_number;


    void EndOfEvent();

    virtual void PreUserTrackingAction (const G4Track *);
    virtual void PostUserTrackingAction (const G4Track *);
};


#endif // TRACKING_ACTION_HH
