#ifndef HIT_HH
#define HIT_HH 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"

#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"

#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4String.hh"
#include "G4Colour.hh"


class G4VTouchable;

//--------------------------------------------------
// MHit Class
//--------------------------------------------------

class MHit : public G4VHit {
public:

    MHit();

    virtual ~MHit();

    MHit(const MHit &right);

    void Draw();

    const MHit &operator=(const MHit &right);

    G4int operator==(const MHit &right) const;

    inline void *operator new(size_t);

    inline void operator delete(void *aHit);

    inline void SetEDep(G4double eDep_){eDep = eDep_;}
    inline G4double GetEDep(){return eDep;}

    inline void SetETot(G4double eTot_){eTot = eTot_;}
    inline G4double GetETot(){return eTot;}

    inline void SetTime(G4double time_){time = time_;}
    inline G4double GetTime(){return time;}

    inline void SetTrackID(G4int trackID_){trackID = trackID_;}
    inline G4int GetTrackID(){return trackID;}

    inline void SetParticleID(G4int particleID_){particleID = particleID_;}
    inline G4int GetParticleID(){return particleID;}

    inline void SetProcessName(G4String processName_){processName = processName_;}
    inline G4String GetProcessName(){return processName;}

    inline void SetVolumeName(G4String volumeName_){volumeName = volumeName_;}
    inline G4String GetVolumeName(){return volumeName;}

    inline void SetPos(G4ThreeVector pos_){pos = pos_;}
    inline G4ThreeVector GetPos(){return pos;}

    inline void SetLocalPos(G4ThreeVector localPos_){localPos = localPos_;}
    inline G4ThreeVector GetLocalPos(){return localPos;}


private:
    G4double eDep;
    G4double eTot;
    G4double time;
    G4int particleID;
    G4int trackID;
    G4String processName;
    G4String volumeName;
    G4ThreeVector pos;
    G4ThreeVector localPos;

    G4Colour colourHit, colourPassby;

};

//--------------------------------------------------
// Type Definitions
//--------------------------------------------------

typedef G4THitsCollection <MHit> HitsCollection;

extern G4ThreadLocal G4Allocator<MHit>* HitAllocator;

//--------------------------------------------------
// Operator Overloads
//--------------------------------------------------

inline void *MHit::operator new(size_t) {
    if (!HitAllocator)
        HitAllocator = new G4Allocator<MHit>;
    return (void *) HitAllocator->MallocSingle();
}

inline void MHit::operator delete(void *aHit) {
    HitAllocator->FreeSingle((MHit *) aHit);
}

#endif
