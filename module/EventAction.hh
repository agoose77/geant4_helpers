#ifndef EVENT_ACTION_HH
#define EVENT_ACTION_HH

#include "TrackingAction.hh"
#include "G4UserEventAction.hh"


class EventAction: public G4UserEventAction
{
public:
    EventAction(TrackingAction*);
    ~EventAction();
    
    virtual void EndOfEventAction(const G4Event*);
    
private:
    TrackingAction* trackingAction;
    
};


#endif // EVENT_ACTION_HH