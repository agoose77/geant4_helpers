# Geant4 helpers module
Defines the `detector` module, which provides certain custom C++ (with boost.python bindings) definitions for user actions.